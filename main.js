var moveSound;
let muted = false;

function setup() {
  moveSound = loadSound("sound/woodblock.mp3");
  moveSound.setVolume(0.25);
}

const playerRed = makePlayer("playerRed");
const playerBlack = makePlayer("playerBlack");
function makePlayer(playerName) {
  const player = document.createElement("div");
  player.className = playerName;
  return player;
}

const column1 = makeColumn("column1");
const column2 = makeColumn("column2");
const column3 = makeColumn("column3");
const column4 = makeColumn("column4");
const column5 = makeColumn("column5");
const column6 = makeColumn("column6");
const column7 = makeColumn("column7");
function makeColumn(columnName) {
  const column = document.createElement("div");
  column.className = columnName;
  return column;
}

const stand1 = document.createElement("div");
stand1.className = "stand1";
const stand2 = document.createElement("div");
stand2.className = "stand2";

const columnContainer = document.createElement("div");
columnContainer.className = "columns";
const columns = [column1, column2, column3, column4, column5, column6, column7];

columnContainer.appendChild(stand1);
for (let i = 0; i < columns.length; i++) {
  columnContainer.appendChild(columns[i]);
}
columnContainer.appendChild(stand2);

let destination = document.getElementById("d1");
destination.appendChild(columnContainer);

let column1Arr = [0, 0, 0, 0, 0, 0];
let column2Arr = [0, 0, 0, 0, 0, 0];
let column3Arr = [0, 0, 0, 0, 0, 0];
let column4Arr = [0, 0, 0, 0, 0, 0];
let column5Arr = [0, 0, 0, 0, 0, 0];
let column6Arr = [0, 0, 0, 0, 0, 0];
let column7Arr = [0, 0, 0, 0, 0, 0];

let board = [
  column1Arr,
  column2Arr,
  column3Arr,
  column4Arr,
  column5Arr,
  column6Arr,
  column7Arr
];

function addValueToArray(column) {
  let pointValue = currentPlayerIsRed ? 1 : 2;
  for (let i = 0; i < columns.length; i++) {
    if (column === columns[i]) {
      let firstZero = board[i].findIndex(function(element) {
        return element === 0;
      });
      board[i][firstZero] = board[i][firstZero] + pointValue;
    }
  }
}

let gameOver = false;
let winnerText = document.getElementById("winner");

winnerText.innerHTML = "Red's turn";

let currentPlayerIsRed = true;
const handleClick = function(event) {
  if (gameOver === false) {
    let selectedColumn = event.currentTarget;
    moveSound.play();
    if (selectedColumn.childElementCount <= 5) {
      if (currentPlayerIsRed === true) {
        let discRed = document.createElement("div");
        discRed.className = "playerRed";
        addValueToArray(selectedColumn);
        selectedColumn.appendChild(discRed);
        winnerText.innerHTML = "Black's turn";
      } else {
        let discBlack = document.createElement("div");
        discBlack.className = "playerBlack";
        addValueToArray(selectedColumn);
        selectedColumn.appendChild(discBlack);
        winnerText.innerHTML = "Red's turn";
      }
      checkWinner();
    }
  }
  currentPlayerIsRed = !currentPlayerIsRed;
};

columns.forEach(function(column) {
  column.addEventListener("click", handleClick);
});

const edgeX = 4; //board.length - 3 = 7 - 3
const edgeY = 3; //board[0].length - 3 = 6 -3

function winningText() {
  if (currentPlayerIsRed === true) {
    winnerText.innerHTML = "Red Wins!";
  } else {
    winnerText.innerHTML = "Black Wins!";
  }
}

function checkWinnerVertical() {
  for (let i = 0; i < board.length; i++) {
    for (let j = 0; j < 3; j++) {
      if (board[i][j] !== 0) {
        if (
          board[i][j] === board[i][j + 1] &&
          board[i][j] === board[i][j + 2] &&
          board[i][j] === board[i][j + 3]
        ) {
          winningText();
          gameOver = true;
        }
      }
    }
  }
}

function checkWinnerHorizontal() {
  for (let i = 0; i < edgeX; i++) {
    for (let j = 0; j < board[0].length; j++) {
      if (board[i][j] !== 0) {
        if (
          board[i][j] === board[i + 1][j] &&
          board[i][j] === board[i + 2][j] &&
          board[i][j] === board[i + 3][j]
        ) {
          winningText();
          gameOver = true;
        }
      }
    }
  }
}

function checkWinnerDiagonalUpRight() {
  for (let i = 0; i < edgeX; i++) {
    for (let j = 0; j < edgeY; j++) {
      if (board[i][j] !== 0) {
        if (
          board[i][j] === board[i + 1][j + 1] &&
          board[i][j] === board[i + 2][j + 2] &&
          board[i][j] === board[i + 3][j + 3]
        ) {
          winningText();
          gameOver = true;
        }
      }
    }
  }
}

function checkWinnerDiagonalUpLeft() {
  for (let i = 3; i < board[0].length; i++) {
    for (let j = 0; j < edgeY; j++) {
      if (board[i][j] !== 0) {
        if (
          board[i][j] === board[i - 1][j + 1] &&
          board[i][j] === board[i - 2][j + 2] &&
          board[i][j] === board[i - 3][j + 3]
        ) {
          winningText();
          gameOver = true;
        }
      }
    }
  }
}

function checkTie() {
  let counter = 0;
  for (let i = 0; i < board.length; i++) {
    if (board[i].includes(0) === false) {
      counter++;
      if (counter == 7) {
        winnerText.innerHTML = "Draw";
        gameOver = true;
      }
    }
  }
}

function checkWinner() {
  checkWinnerVertical();
  checkWinnerHorizontal();
  checkWinnerDiagonalUpRight();
  checkWinnerDiagonalUpLeft();
  checkTie();
}

const button = document.getElementById("resetButton");
button.onclick = function() {
  location.reload();
};

const muteButton = document.getElementById("muteButton");
muteButton.onclick = function() {
  if (muted === false) {
    muteButton.innerHTML = "Unmute";
    moveSound.setVolume(0);
    muted = true;
  } else {
    muteButton.innerHTML = "Mute";
    moveSound.setVolume(1);
    muted = false;
  }
};
