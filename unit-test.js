function testcheckWinnerHorizontal() {
  board[0][0] = 1;
  board[1][0] = 1;
  board[2][0] = 1;
  board[3][0] = 1;
  checkWinnerHorizontal();
  console.assert(gameOver === true, {
    function: "checkWinnerHorizontal()",
    expected: true,
    result: gameOver
  });
}

function testcheckWinnerVertical() {
  board[6][0] = 1;
  board[6][1] = 1;
  board[6][2] = 1;
  board[6][3] = 1;
  checkWinnerVertical();
  console.assert(gameOver === true, {
    function: "checkWinnerVertical()",
    expected: true,
    result: gameOver
  });
}
//test.checkwinnerVertical
//testcheckWinnerHorizontal();
//testcheckWinnerVertical();
